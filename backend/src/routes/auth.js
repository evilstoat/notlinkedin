import express from 'express';
import jwt from 'jsonwebtoken';
import executeQuery from '../utils/executeQuery';
import authenticateToken from '../utils/useAuth';

const JWT_SECRET = 'notlinkedinkey';

const authRouter = express.Router();

// Register Endpoint
authRouter.post('/register', async (req, res) => {
  const {
    email, password, firstName, secondName,
  } = req.body;

  if (!email || !password || !firstName || !secondName) {
    return res.status(400).json({ message: 'Email, пароль, имя и фамилия обязательны для регистрации!' });
  }

  try {
    // Check if user already exists
    const existingData = await executeQuery(`SELECT email FROM Profile WHERE email = '${email}';`);
    if (existingData.length > 0) {
      return res.status(409).json({ message: 'User with this email already exists' });
    }

    // Insert new user into the database
    await executeQuery(`INSERT INTO Profile (email, password, first_name, second_name) VALUES ('${email}', '${password}', '${firstName}', '${secondName}');`);

    // Generate JWT
    const accessToken = jwt.sign(
      { email },
      JWT_SECRET,
      { expiresIn: '1d' }, // expires in 24 hours
    );

    // Response with token
    return res.status(201).json({
      message: 'User registered successfully',
      accessToken,
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ message: 'Database error during registration' });
  }
});

// Login Endpoint
authRouter.post('/login', async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).json({ message: 'Email and password are required' });
  }

  try {
    const data = await executeQuery(`SELECT * FROM Profile WHERE email = '${email}';`);
    const user = data[0];

    if (!user) {
      return res.status(401).json({ message: 'Invalid credentials' });
    }

    // Check password
    if (password === user.password) {
      // Generate JWT
      const accessToken = jwt.sign(
        { id: user.id, email: user.email },
        JWT_SECRET,
        { expiresIn: '1d' }, // expires in 24 hours
      );

      return res.json({ accessToken });
    }
    return res.status(401).json({ message: 'Invalid credentials' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Database error' });
  }

  return null;
});

// A protected route example
authRouter.get('/protected', authenticateToken, (req, res) => {
  res.json({ message: 'Protected data', user: req.user });
});

export default authRouter;
