import express from 'express';
import executeQuery from '../utils/executeQuery';

const companyRouter = express.Router();

// GET all companies
companyRouter.get('/', async (_, res) => {
  const data = await executeQuery('SELECT * FROM Company ORDER BY id;');
  res.json(data);
});

// POST a new company
companyRouter.post('/', async (req, res) => {
  try {
    const { body } = req;
    const keys = Object.keys(body);
    const values = keys.map((key) => `'${body[key].replace(/'/g, "''")}'`).join(', ');

    await executeQuery(`INSERT INTO Company(${keys.join(', ')}) VALUES(${values});`);
    const data = await executeQuery(`SELECT * FROM Company WHERE name='${body.name.replace(/'/g, "''")}';`);

    res.status(201).json(data[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// GET a single company by ID
companyRouter.get('/:id', async (req, res) => {
  const { id } = req.params;
  const data = await executeQuery(`SELECT * FROM Company WHERE id=${id};`);
  if (data.length) {
    res.json(data[0]);
  } else {
    res.status(404).send('Company not found');
  }
});

// PUT (update) a company by ID
companyRouter.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const updates = Object.keys(body)
      .map((key) => `${key}='${body[key].replace(/'/g, "''")}'`)
      .join(', ');

    await executeQuery(`UPDATE Company SET ${updates} WHERE id=${id};`);
    const updatedData = await executeQuery(`SELECT * FROM Company WHERE id=${id};`);

    res.json(updatedData[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// DELETE a company by ID
companyRouter.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    await executeQuery(`DELETE FROM Company WHERE id=${id};`);

    res.status(204).send();
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// GET workers
companyRouter.get('/:companyId/workers', async (req, res) => {
  try {
    const { companyId } = req.params;

    const response = await executeQuery(`select profile.*, company.name, company.industry, company.country from profile left join company company on profile.company_id=company.id where company_id='${companyId}'`);

    res.status(200).send(response);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

export default companyRouter;
