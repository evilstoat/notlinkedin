import express from 'express';
import profileRouter from './profile';
import companyRouter from './company';
import authRouter from './auth';
import messageRouter from './message';
import uploadRouter from './upload';
import wallMessageRouter from './wallmessage';

const router = express.Router();

router.use('/auth', authRouter);
router.use('/profile', profileRouter);
router.use('/company', companyRouter);
router.use('/message', messageRouter);
router.use('/wallmessage', wallMessageRouter);
router.use('/upload', uploadRouter);

export default router;
