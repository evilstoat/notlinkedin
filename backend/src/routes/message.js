import express from 'express';
import executeQuery from '../utils/executeQuery';
import authenticateToken from '../utils/useAuth';

const messageRouter = express.Router();

messageRouter.get('/common-profiles', authenticateToken, async (req, res) => {
  try {
    const userId = req.user.id;

    const query = `
      SELECT 
        p.id, 
        p.first_name, 
        p.second_name, 
        p.photo, 
        COUNT(m.id) AS message_count,
        (
          SELECT message
          FROM Message
          WHERE (profile_receiver_id = p.id OR profile_sender_id = p.id) AND
                (profile_sender_id = ${userId} OR profile_receiver_id = ${userId})
          ORDER BY date DESC
          LIMIT 1
        ) AS last_message,
        (
          SELECT date
          FROM Message
          WHERE (profile_receiver_id = p.id OR profile_sender_id = p.id) AND
                (profile_sender_id = ${userId} OR profile_receiver_id = ${userId})
          ORDER BY date DESC
          LIMIT 1
        ) AS last_message_date
      FROM 
        Profile p
      LEFT JOIN 
        Message m 
      ON 
        m.profile_receiver_id = p.id OR m.profile_sender_id = p.id
      WHERE 
        (m.profile_sender_id = ${userId} OR m.profile_receiver_id = ${userId}) AND p.id != ${userId}
      GROUP BY 
        p.id, p.first_name, p.second_name, p.photo
      ORDER BY 
        message_count ASC;
    `;

    const profiles = await executeQuery(query);
    if (profiles.length) {
      res.json(profiles);
    } else {
      res.status(404).send('No profiles found with common messages.');
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

messageRouter.post('/conversation', authenticateToken, async (req, res) => {
  try {
    const currentUserId = req.user.id; // This assumes the JWT token includes the user ID as 'id'
    
    const { userId: otherUserId } = req.body;
    console.log(currentUserId)
    console.log(otherUserId)

    const query = `
        SELECT m.id, m.message, m.date,
            ps.id AS sender_id, ps.first_name AS sender_first_name, ps.second_name AS sender_second_name, ps.photo AS sender_photo,
            pr.id AS receiver_id, pr.first_name AS receiver_first_name, pr.second_name AS receiver_second_name, pr.photo AS receiver_photo
        FROM Message m
        JOIN Profile ps ON m.profile_sender_id = ps.id
        JOIN Profile pr ON m.profile_receiver_id = pr.id
        WHERE (m.profile_sender_id = ${currentUserId} AND m.profile_receiver_id = ${otherUserId})
            OR (m.profile_sender_id = ${otherUserId} AND m.profile_receiver_id = ${currentUserId})
        ORDER BY m.date asc;
      `;

    const data = await executeQuery(query);
    if (data.length > 0) {
      res.json(data);
    } else {
      res.status(404).send([]);
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// GET all messages
messageRouter.get('/', async (_, res) => {
  const data = await executeQuery('SELECT * FROM Message ORDER BY date DESC;');
  res.json(data);
});

// POST a new message
messageRouter.post('/', authenticateToken, async (req, res) => {
  try {
    const { body } = req;
    const keys = ['profile_sender_id', 'profile_receiver_id', 'message'];
    const values = keys.map((key) => `'${body[key]}'`).join(', ');

    await executeQuery(`INSERT INTO Message(${keys.join(', ')}) VALUES(${values});`);

    // Feedback with the latest entry, assuming the last message sent is what we want
    const data = await executeQuery('SELECT * FROM Message ORDER BY id DESC LIMIT 1;');
    res.status(201).json(data[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// GET a single message by ID
messageRouter.get('/:id', async (req, res) => {
  const { id } = req.params;
  const data = await executeQuery(`SELECT * FROM Message WHERE id=${id};`);
  if (data.length) {
    res.json(data[0]);
  } else {
    res.status(404).send('Message not found');
  }
});

// PUT (update) a message by ID
messageRouter.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const keys = ['message', 'date']; // Assume these are the only updateable fields
    const updates = keys.map((key) => `${key}='${body[key]}'`).join(', ');

    await executeQuery(`UPDATE Message SET ${updates} WHERE id=${id};`);
    const updatedData = await executeQuery(`SELECT * FROM Message WHERE id=${id};`);

    res.json(updatedData[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// DELETE a message by ID
messageRouter.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    await executeQuery(`DELETE FROM Message WHERE id=${id};`);

    res.status(204).send();
  } catch (e) {
    res.status(500).send(e.message);
  }
});

export default messageRouter;
