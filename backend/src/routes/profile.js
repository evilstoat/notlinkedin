import express from 'express';
import executeQuery from '../utils/executeQuery';
import authenticateToken from '../utils/useAuth';

const profileRouter = express.Router();

profileRouter.get('/me', authenticateToken, async (req, res) => {
  try {
    const { user } = req;

    const response = await executeQuery(
      `select profile.*, company.name, company.industry, company.country from profile left join company company on profile.company_id=company.id where email='${user.email}'`,
    );

    res.status(200).send(response[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// POST a new profile
profileRouter.post('/', async (req, res) => {
  try {
    const { body } = req;
    const keys = Object.keys(body);
    const values = keys.map((key) => `'${body[key]}'`).join(', ');

    await executeQuery(
      `INSERT INTO Profile(${keys.join(', ')}) VALUES(${values});`,
    );
    const data = await executeQuery(
      `SELECT * FROM Profile WHERE email='${body.email}';`,
    );

    res.status(201).json(data[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// GET all profiles
profileRouter.get('/', async (req, res) => {
  const data = await executeQuery(
    'select profile.id as id, profile.*, company.name, company.industry, company.country from profile left join company company on profile.company_id=company.id',
  );
  if (data.length) {
    res.json(data);
  } else {
    res.status(404).send('Profile not found');
  }
});

// GET a single profile by ID
profileRouter.get('/id/:id', async (req, res) => {
  const { id } = req.params;
  const data = await executeQuery(`select profile.id as id, profile.*, company.name, company.industry, company.country from profile left join company company on profile.company_id=company.id WHERE profile.id=${id}`);
  if (data.length) {
    res.json(data[0]);
  } else {
    res.status(404).send('Profile not found');
  }
});

// PUT (update) a profile by ID
profileRouter.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { body } = req;

    const updates = Object.keys(body)
      .map((key) => {
        const valueToSet = body[key] === null ? body[key] : `'${body[key]}'`;

        return `${key}=${valueToSet}`;
      })
      .join(', ');

    await executeQuery(`UPDATE Profile SET ${updates} WHERE id=${id};`);
    const updatedData = await executeQuery(
      `SELECT * FROM Profile WHERE id=${id};`,
    );

    res.json(updatedData[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// DELETE a profile by ID
profileRouter.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    await executeQuery(`DELETE FROM Profile WHERE id=${id};`);

    res.status(204).send();
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// DELETE a profile by ID
profileRouter.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    await executeQuery(`DELETE FROM Profile WHERE id=${id};`);

    res.status(204).send();
  } catch (e) {
    res.status(500).send(e.message);
  }
});

export default profileRouter;
