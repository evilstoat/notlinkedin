import express from 'express';
import multer from 'multer';

import path from 'path';
import fs from 'fs';

const uploadRouter = express.Router();

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/');
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.originalname.split('.').pop()}`);
  },
});

const upload = multer({ storage });

uploadRouter.post('/', upload.single('image'), (req, res, next) => {
  try {
    const { file } = req;
    if (!file) {
      const error = new Error('Please upload a file');
      error.httpStatusCode = 400;
      return next(error);
    }

    const url = `http://localhost:8080/api/upload/image/${file.filename}`;

    res.send({ ...file, url });
  } catch (e) {
    res.status(500).send(e.message);
  }

  return null;
});

uploadRouter.get('/image/:filename', (req, res) => {
  const { filename } = req.params;
  const dirname = path.resolve('./uploads');
  const filepath = path.join(dirname, filename);

  // Check if file exists
  fs.access(filepath, fs.constants.F_OK, (err) => {
    if (err) {
      console.error('File does not exist');
      return res.status(404).send('File not found');
    }
    // Serve file using a stream
    const filestream = fs.createReadStream(filepath);
    filestream.pipe(res);
  });

  return null;
});

export default uploadRouter;
