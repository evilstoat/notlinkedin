import express from 'express';
import executeQuery from '../utils/executeQuery';
import authenticateToken from '../utils/useAuth';

const wallMessageRouter = express.Router();

// GET all messages
wallMessageRouter.get('/', async (_, res) => {
  try {
    const data = await executeQuery('SELECT * FROM WallMessage ORDER BY id;');
    res.json(data);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// POST a new message
wallMessageRouter.post('/', async (req, res) => {
  try {
    const { profile_sender_id, profile_receiver_id, message } = req.body;
    const queryParams = [
      profile_sender_id,
      profile_receiver_id,
      message.replace(/'/g, "''"),
    ];

    const query = `INSERT INTO WallMessage(profile_sender_id, profile_receiver_id, message) VALUES(${profile_sender_id}, ${profile_receiver_id}, '${message}');`;
    const data = await executeQuery(query);

    res.status(201).json(data[0]);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// GET a single message by ID
wallMessageRouter.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const data = await executeQuery('SELECT * FROM WallMessage WHERE id=$1;', [
      id,
    ]);
    if (data.length) {
      res.json(data[0]);
    } else {
      res.status(404).send('Message not found');
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// PUT (update) a message by ID
wallMessageRouter.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { profile_sender_id, profile_receiver_id, message } = req.body;
    const queryParams = [
      profile_sender_id,
      profile_receiver_id,
      message.replace(/'/g, "''"),
      id,
    ];

    const updateQuery = 'UPDATE WallMessage SET profile_sender_id=$1, profile_receiver_id=$2, message=$3 WHERE id=$4 RETURNING *;';
    const updatedData = await executeQuery(updateQuery, queryParams);

    if (updatedData.length) {
      res.json(updatedData[0]);
    } else {
      res.status(404).send('Message not found');
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// DELETE a message by ID
wallMessageRouter.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    await executeQuery('DELETE FROM WallMessage WHERE id=$1;', [id]);
    res.status(204).send();
  } catch (e) {
    res.status(500).send(e.message);
  }
});

wallMessageRouter.get('/profile/:id', authenticateToken, async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;

  try {
    const data = await executeQuery(`
        SELECT * FROM WallMessage
        WHERE (profile_receiver_id = ${id} OR profile_sender_id = ${id}) AND
        (profile_sender_id = ${userId} OR profile_receiver_id = ${userId})
        order by id desc;
    `);
    res.json(data);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

export default wallMessageRouter;
