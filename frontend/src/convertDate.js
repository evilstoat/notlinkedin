function formatDate(isoDate) {
  const inputDate = new Date(isoDate)
  const currentDate = new Date()

  // Resetting the time part of currentDate to make date comparison accurate
  currentDate.setHours(0, 0, 0, 0)

  // Create a new date object for yesterday
  const yesterdayDate = new Date(currentDate)
  yesterdayDate.setDate(currentDate.getDate() - 1)

  // Comparing dates
  if (inputDate.toDateString() === currentDate.toDateString()) {
    // Return the time part when the date is today
    return inputDate.toTimeString().slice(0, 5)
  } else if (inputDate.toDateString() === yesterdayDate.toDateString()) {
    // Return 'yesterday' if the date matches yesterday
    return 'Вчера'
  } else {
    // Otherwise, format the date as day.month.year
    const day = inputDate.getDate().toString().padStart(2, '0')
    const month = (inputDate.getMonth() + 1).toString().padStart(2, '0')
    const year = inputDate.getFullYear()
    return `${day}.${month}.${year}`
  }
}

export default formatDate