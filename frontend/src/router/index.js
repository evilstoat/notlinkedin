import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'AuthLogin',
      component: () => import('../views/AuthLogin.vue')
    },
    {
      path: '/registration',
      name: 'AuthRegistration',
      component: () => import('../views/AuthRegistration.vue'),
    },
    {
      path: '/profile-edit',
      name: 'ProfileEdit',
      component: () => import('../views/ProfileEdit.vue'),
    },
    {
      path: '/profile/:id',
      name: 'ProfileDetails',
      component: () => import('../views/ProfileDetails.vue'),
    },
    {
      path: '/messages',
      name: 'MyMessage',
      component: () => import('../views/MyMessage.vue'),
    },
    {
      path: '/messages/:id',
      name: 'MessageWithPerson',
      component: () => import('../views/MessageWithPerson.vue'),
    },
    {
      path: '/company/:id',
      name: 'CompanyDetails',
      component: () => import('../views/CompanyDetails.vue'),
    },
    {
      path: '/company',
      name: 'CompanyList',
      component: () => import('../views/CompanyList.vue'),
    }
  ]
})

export default router
