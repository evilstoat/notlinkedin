import { defineStore } from 'pinia'
import axios from 'axios'

export const useAuthStore = defineStore('auth', {
  state: () => ({
    accessToken: localStorage.getItem('accessToken') || null,
    userData: null
  }),
  getters: {
    isLoggedIn: (state) => !!state.accessToken
  },
  actions: {
    async register({ email, password, firstName, secondName }) {
      try {
        const response = await axios.post('http://localhost:8080/api/auth/register', {
          email,
          password,
          firstName,
          secondName
        })
        this.login({ email, password })
        return response.data
      } catch (error) {
        throw new Error('Registration Failed: ' + (error.response.data.message || error.message))
      }
    },
    async login({ email, password }) {
      try {
        const response = await axios.post('http://localhost:8080/api/auth/login', {
          email,
          password
        })
        this.accessToken = response.data.accessToken
        localStorage.setItem('accessToken', this.accessToken)
        this.fetchUserData()
        return response.data
      } catch (error) {
        throw new Error('Login Failed: ' + (error.response.data.message || error.message))
      }
    },
    async fetchUserData() {
      if (!this.accessToken) return

      try {
        const response = await axios.get('http://localhost:8080/api/profile/me', {
          headers: {
            Authorization: `Bearer ${this.accessToken}`
          }
        })
        this.userData = response.data
      } catch (error) {
        console.error('Failed to fetch user data:', error.message)
        this.logout()
      }
    },
    logout() {
      this.accessToken = null
      this.userData = null
      localStorage.removeItem('accessToken')
    }
  }
})
